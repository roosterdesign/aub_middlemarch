<?php get_header(); ?>
	<div id="wrap">
		<?php
			$hero = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'hero'); $hero = $hero['0'];
			$title = get_the_title();
		?>
		<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
		<section class="main-content">
			<div class="container">
				<main>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				</main>
				<?php include(get_template_directory()."/page-templates/inc/sidebar.php"); ?>
			</div>
		</section>
		<?php include(get_template_directory()."/page-templates/inc/case-studies.php"); ?>	
    </div>  
<?php get_footer(); ?>