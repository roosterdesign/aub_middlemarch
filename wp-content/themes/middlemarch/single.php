<?php get_header(); ?>
	<div id="wrap">
		<?php $hero = wp_get_attachment_image_src(get_post_thumbnail_id(12), 'hero'); $hero = $hero['0']; ?>
		<div id="hero" class="img" style="background-image:url(<?php echo $hero; ?>);">
			<div class="container">
				<div class="inner">
					<h1><?php echo get_the_title(12); ?></h1>
				</div>
			</div>
		</div>
		<div class="meta">
			<div class="container">
				<a href="/blog">Back to <?php echo get_the_title(12); ?></a>
				<span class="date"><?php echo get_the_date( 'j<\s\up>S</\s\up> F Y'); ?></span>
				<?php echo get_the_category_list(); ?>
			</div>
		</div>
		<div class="container narrow">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>
		</div>
	</div>
<?php get_footer(); ?>