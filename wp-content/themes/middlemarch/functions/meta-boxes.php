<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */

/********************* META BOX DEFINITIONS ***********************/

/**
 * Prefix of meta keys (optional)
 * Use underscore (_) at the beginning to make keys hidden
 * Alt.: You also can make prefix empty to disable it
 */
// Better has an underscore as last sign
$prefix = 'CS_';

global $meta_boxes;

$meta_boxes = array();




// top banners
$meta_boxes[] = array(
  'id' => 'topbannermeta',
  'title' => __( 'Banner Images', 'rwmb' ),
  'pages' => array( 'topbanners' ),
  'context' => 'normal',
  'priority' => 'high',
  'autosave' => true,
  'fields' => array(

      // PLUPLOAD IMAGE UPLOAD (WP 3.3+)
      array(
        'name'             => __( 'Upload Images', 'rwmb' ),
        'id'               => "{$prefix}bannerPlupload",
        'type'             => 'plupload_image',
        'max_file_uploads' => 99,
      ),

  ), // end fields
);






// page chooser for the top banner
$meta_boxes[] = array(
  'id' => 'choosepagebanner',
  'title' => __( 'Top Banner Images', 'rwmb' ),
  'pages' => array( 'page', 'service' ),
  'context' => 'normal',
  'priority' => 'low',
  'autosave' => true,
  'fields' => array(

     // POST
        array(
          'name'    => 'Select Top Banner (if applicable)',
          'id'      => "{$prefix}pickBanners",
          'type'    => 'post',

          // Post type
          'post_type' => 'topbanners',
          // Field type, either 'select' or 'select_advanced' (default)
          'field_type' => 'select_advanced',
          // Query arguments (optional). No settings means get all published posts
          'query_args' => array(
            'post_status' => 'publish',
            'posts_per_page' => '-1',
            'orderby'   => 'menu_order',
            'order'     => 'ASC'
          )
        ),


  ), // end fields
);





// galleries 

$meta_boxes[] = array(
  'id' => 'maingallery',
  'title' => 'Gallery Images',
  'pages' => array( 'galleries' ),
  'context' => 'normal',
  'priority' => 'high',
  'autosave' => true,
  'fields' => array(

      // PLUPLOAD IMAGE UPLOAD (WP 3.3+)
      array(
        'name'             => 'Upload Images',
        'id'               => "{$prefix}galleryPlupload",
        'type'             => 'plupload_image',
        'max_file_uploads' => 999,
      ),

  ), // end fields
);






// service details page
$meta_boxes[] = array(
  'id' => 'servicemetastuff',
  'title' => 'Additional Service Information',
  'pages' => array( 'service' ),
  'context' => 'normal',
  'priority' => 'high',
  'autosave' => false,
  'fields' => array(


    // TEXTAREA
    array(
      'name' => 'Parent Page Caption',
      'desc' => 'Summary Text for the Service Page',
      'id'   => "{$prefix}summarytextarea",
      'type' => 'textarea',
      'cols' => 20,
      'rows' => 8,
    ),


  ), // end fields
);





// testimonial details page
$meta_boxes[] = array(
  'id' => 'testimonialmetastuff',
  'title' => 'Additional Testimonial Information',
  'pages' => array( 'testimonial' ),
  'context' => 'normal',
  'priority' => 'high',
  'autosave' => false,
  'fields' => array(



    // SELECT BOX
    array(
      'name'     => 'Testimonial Type',
      'id'       => "{$prefix}testimonial_type",
      'type'     => 'select',
      // Array of 'value' => 'Label' pairs for select box
      'options'  => array(
        'image' => __( 'Image', 'rwmb' ),
        'video' => __( 'Video', 'rwmb' ),
      ),
      'multiple' => false,
      'std' => __( 'Select an Item', 'rwmb' ),
      'desc' => 'Select whether to pull through the featured image or a video',
    ),


    // URL
    // array(
    //   'name'  => 'URL',
    //   'id'    => "{$prefix}testimonial_video_url",
    //   'desc'  => 'URL description',
    //   'type'  => 'url',
    //   'std'   => 'http://google.com',
    // ),
    // OEMBED
    array(
      'name'  => 'YouTube Share URL',
      'id'    => "{$prefix}testimonial_video_oembed",
      'desc'  => 'Enter the YouTube Share URL',
      'type'  => 'oembed',
    ),



  ), // end fields
);





/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function CS_register_meta_boxes()
{
  // Make sure there's no errors when the plugin is deactivated or during upgrade
  if ( !class_exists( 'RW_Meta_Box' ) )
    return;

  global $meta_boxes;
  foreach ( $meta_boxes as $meta_box )
  {
    new RW_Meta_Box( $meta_box );
  }
}
// Hook to 'admin_init' to make sure the meta box class is loaded before
// (in case using the meta box class in another plugin)
// This is also helpful for some conditionals like checking page template, categories, etc.
add_action( 'admin_init', 'CS_register_meta_boxes' );
