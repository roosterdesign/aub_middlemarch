<?php 

/*--- SORT OUT THE REQUIRED TYPES OF IMAGE SIZES ---*/ 
 
if ( function_exists( 'add_theme_support' ) ) { 
  add_theme_support( 'post-thumbnails' ); 
}

if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );

if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'blog_thumbnail', '710', '350', true);
	add_image_size( 'hero', '2560', '640', true);
  add_image_size( 'hero_split', '1280', '640', true);
  add_image_size( 'hero_circle', '260', '260', true);
	add_image_size( 'blog_hero', '2560', '300', true);
  add_image_size( 'cs_banner_bg', '400', '315', true);
  add_image_size( 'leading_banner_image', '1200', '315', true);
  add_image_size( 'block_image', '590', '315', true);
}


function remove_default_image_sizes( $sizes) {
    unset( $sizes['thumbnail']);
    unset( $sizes['medium']);
    unset( $sizes['medium_large']);
    //unset( $sizes['large']);
    unset( $sizes['full']);
    return $sizes;
}

add_filter('intermediate_image_sizes_advanced', 'remove_default_image_sizes');
add_filter('jpeg_quality', function($arg){return 65;});
add_filter( 'wp_editor_set_quality', function($arg){return 65;} );

?>