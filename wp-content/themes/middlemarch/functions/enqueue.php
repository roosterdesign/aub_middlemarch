<?php 

    // ===============================================================================
    // ENQUEUE STYLES
    // ===============================================================================
    
    if (!is_admin()) add_action("wp_enqueue_scripts", "aub_script_enqueue", 11);
    function aub_script_enqueue() {

      // Main CSS
      wp_enqueue_style( 'style', get_template_directory_uri() . '/css/dist/styles.min.css' );

      wp_deregister_script( 'wp-embed' );

      // jQuery
      wp_deregister_script( 'jquery' );
      wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js",  false, null, false);
      wp_enqueue_script('jquery');

      // Main JS
      wp_deregister_script('main');
      wp_register_script('main', get_template_directory_uri(). '/js/dist/main.min.js', ['jquery'], null, true);
      wp_enqueue_script('main');

    }


   // ===============================================================================
   // TIDY UP THE SCRIPTS BY REMOVING THE DOMAIN FROM THEM :) 
   // ===============================================================================

  if(!is_admin()) {
    add_filter( 'script_loader_src', 'wpse47206_src' );
    add_filter( 'style_loader_src', 'wpse47206_src' );
    function wpse47206_src( $url ) {
      $url = str_replace( site_url(), '', $url);
      $siteUrlWithoutHttp = str_replace( 'http:', '',  site_url());
      $url = str_replace( site_url(), '', $url);
      $url = str_replace($siteUrlWithoutHttp, '', $url);
      return $url;
    } 
  }


  // ===============================================================================
  // REMOVE UNREQUIRED WP SCRIPTS
  // ===============================================================================

  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' );

?>