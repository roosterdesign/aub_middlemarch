<?php

add_filter( 'login_headerurl', 'namespace_login_headerurl' );
/**
 * Replaces the login header logo URL
 *
 * @param $url
 */
function namespace_login_headerurl( $url ) {
    $url = home_url( '/' );
    return $url;
}
 


add_filter( 'login_headertitle', 'namespace_login_headertitle' );
/**
 * Replaces the login header logo title
 *
 * @param $title
 */
function namespace_login_headertitle( $title ) {
    $title = get_bloginfo( 'name' );
    return $title;
}


add_action("login_head", "my_login_head");
function my_login_head() {
  echo "
  <style>
  body.login #login h1 a {
    background: url('".get_bloginfo('template_url')."/img/login.png') no-repeat scroll center top;
    height: 110px;
    width: 325px;
    background-size: 325px 110px;
  }
  </style>
  ";
}


?>