<?php 
function setup_theme_admin_menus() {  
  add_menu_page('Theme settings', 'Site Settings', 'manage_categories', 'aub_theme_settings');
  add_submenu_page('aub_theme_settings', 'Front Page Elements', 'Front Page', 'manage_categories', 'aub_theme_settings', 'theme_front_page_settings');
}  
  
function theme_front_page_settings() { 

  $phone_number = get_option("phone_number");
  $email_address = get_option("email_address");

  $facebook = get_option("facebook");
  $twitter = get_option("twitter");
  $linkedin = get_option("linkedin");
  $info_box_body = get_option("info_box_body");
  $info_box_link_text = get_option("info_box_link_text");
  $info_box_link = get_option("info_box_link");

  if (isset($_POST["update_settings"])) :
    $phone_number = esc_attr($_POST["phone_number"]);
    update_option("phone_number", $phone_number);
    $email_address = esc_attr($_POST["email_address"]);
    update_option("email_address", $email_address);
    $facebook = esc_attr($_POST["facebook"]);
    update_option("facebook", $facebook);
    $twitter = esc_attr($_POST["twitter"]);
    update_option("twitter", $twitter);
    $linkedin = esc_attr($_POST["linkedin"]);
    update_option("linkedin", $linkedin);
    $info_box_body = esc_attr($_POST["info_box_body"]);
    update_option("info_box_body", $info_box_body);
    $info_box_link_text = esc_attr($_POST["info_box_link_text"]);
    update_option("info_box_link_text", $info_box_link_text);
    $info_box_link = esc_attr($_POST["info_box_link"]);
    update_option("info_box_link", $info_box_link);

    ?>  
    <div id="message" class="updated"><p>Settings saved</p></div>  
  <?php endif; ?>  

  <div class="wrap">  
    <?php screen_icon('themes'); ?> <h2>General Website Settings</h2>  
    <form method="POST" action="">  
      <input type="hidden" name="update_settings" value="Y" />  
      <table class="form-table">  
        

        <tr><th scope="row" colspan="2"><h3 style="margin-bottom:0;">Contact Details</h3></th></tr>
        <tr valign="top">  
          <th scope="row"><label for="phone_number">Phone Number</label></th>  
          <td><input type="text" name="phone_number" value="<?php echo $phone_number; ?>" class="regular-text"/></td> 
        </tr>
         <tr valign="top">  
          <th scope="row"><label for="email_address">Email Address</label></th>  
          <td><input type="text" name="email_address" value="<?php echo $email_address; ?>" class="regular-text"/></td>  
        </tr>

        <tr><th scope="row" colspan="2"><h3 style="margin-bottom:0;">Social</h3></th></tr>
        <tr valign="top">  
          <th scope="row"><label for="facebook">Facebook</label></th>  
          <td><input type="text" name="facebook" value="<?php echo $facebook; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="twitter">Twitter</label></th>  
          <td><input type="text" name="twitter" value="<?php echo $twitter; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="linkedin">LinkedIn</label></th>  
          <td><input type="text" name="linkedin" value="<?php echo $linkedin; ?>" class="regular-text"/></td>  
        </tr>

        <tr><th scope="row" colspan="2"><h3 style="margin-bottom:0;">Info Box</h3></th></tr>
        <tr valign="top">  
          <th scope="row"><label for="info_box_body">Body</label></th>  
          <td><input type="text" name="info_box_body" value="<?php echo $info_box_body; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="info_box_link_text">Link Text</label></th>  
          <td><input type="text" name="info_box_link_text" value="<?php echo $info_box_link_text; ?>" class="regular-text"/></td> 
        </tr>
        <tr valign="top">  
          <th scope="row"><label for="info_box_link">Link</label></th>  
          <td><input type="text" name="info_box_link" value="<?php echo $info_box_link; ?>" class="regular-text"/></td> 
        </tr>
      </table>  
      <p><input type="submit" value="Save settings" class="button-primary"/></p>
    </form>  
  </div>  
<?php  }
add_action("admin_menu", "setup_theme_admin_menus"); ?>