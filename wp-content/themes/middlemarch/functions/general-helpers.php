<?php 


/*--- AUTO GENERATE "READ MORE" ON EXERPTS ---*/

function excerpt_read_more_link($output) {
  global $post;
  return $output . '<a href="'. get_permalink($post->ID) . '"> Read More...</a>';
}
add_filter('the_excerpt', 'excerpt_read_more_link');

global $trimmedAssetPath;
$trimmedAssetPath = str_replace(get_bloginfo('url'), '', get_bloginfo('template_directory'));
 
add_theme_support( 'automatic-feed-links' );


/**
 * Conditional Page/Post Navigation Links
 * http://www.ericmmartin.com/conditional-pagepost-navigation-links-in-wordpress-redux/
 * If more than one page exists, return TRUE.
 */
function show_posts_nav() {
  global $wp_query;
  return ($wp_query->max_num_pages > 1);
}

?>