<?php 

/*--- IMPORT CUSTOM META ---*/ 
 
// include 'meta-url.php';
 
 
/*--- DEFINE CUSTOM POST TYPES ---*/ 
 
 add_action( 'init', 'codex_custom_init' );

 function codex_custom_init() {

$casestudiesLabels = array(
      'name' => _x('Case Studies', 'post type general name'),
      'singular_name' => _x('Case Studies', 'post type singular name'),
      'add_new' => _x('Add New', 'Case Studies'),
      'add_new_item' => __('Add New Case Study'),
      'edit_item' => __('Edit Case Study'),
      'new_item' => __('New Case Study'),
      'all_items' => __('All Case Studies'),
      'view_item' => __('View Case Study'),
      'search_items' => __('Search Case Studies'),
      'not_found' =>  __('No Case Studies found'),
      'not_found_in_trash' => __('No Case Studies found in Trash'), 
      'parent_item_colon' => '',
      'menu_name' => 'Case Studies'
   );

   $casestudiesArgs = array(
      'labels' => $casestudiesLabels,
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true, 
      'show_in_menu' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'our-experience'),
      // 'rewrite' => true,
      'exclude_from_search' => false,
      'capability_type' => 'page',
      'has_archive' => false, 
      'hierarchical' => false,
      'menu_position' => null,
      'menu_icon' => 'dashicons-format-aside',
      // 'taxonomies'  => array( 'category' ),
      'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'revisions', 'post-formats' )
   ); 

   register_post_type('case-studies',$casestudiesArgs);

 }
?>