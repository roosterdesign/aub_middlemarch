<?php
/*
Template Name: Page - Survey Template
*/
?>
<?php get_header(); ?>
	<div id="wrap">
		<?php $hero = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'hero_split'); $hero = $hero['0']; ?>
		<div id="hero" class="split">
			<div class="intro">
				<div class="inner">
				<?php if (get_field('survey_circle_image')):
						$circleImage = get_field('survey_circle_image')['sizes'][ 'hero_circle' ];						
					else:
						$circleImage = '/wp-content/uploads/2017/03/circle-fallback.jpg';
					endif; ?>
					<img src="<?php echo $circleImage; ?>">
					<h2>Find out about</h2>
					<ul>
						<?php if (get_field('survey_what_we_do')): ?><li><a href="#what-we-do" class="btn double-border">What we do</a></li><?php endif; ?>
						<?php if (get_field('survey_mitigation')): ?><li><a href="#mitigation" class="btn double-border">Mitigation</a></li><?php endif; ?>
						<?php if (get_field('survey_services')): ?><li><a href="#services" class="btn double-border">Services</a></li><?php endif; ?>
					</ul>
					<?php $post_objects = get_field('select_case_studies'); if( $post_objects ): ?><p class="case-studies-link"><a href="#case-studies" class="btn btn-basic">Case Studies</a></p><?php endif; wp_reset_postdata(); ?>
				</div>
			</div>
			<div class="img" style="background-image:url(<?php echo $hero; ?>);">
				<div class="container">
					<div class="inner">
						<h1><?php the_title(); ?></h1>
						<?php the_field('hero_body'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php if ( function_exists('yoast_breadcrumb') ) : yoast_breadcrumb(' <div class="breadcrumbs"><div class="container"><p>','</p></div></div>'); endif; ?>
		<section class="main-content">
			<div class="container">
				<main>
					<?php if (get_field('survey_what_we_do')): ?>
						<div id="what-we-do">
							<?php /* if (!get_field('survey_mitigation')): include(get_template_directory()."/page-templates/inc/info-box.php"); endif; */ ?>
							<?php the_field('survey_what_we_do'); ?>
						</div>
					<?php endif; ?>
					<?php if (get_field('survey_mitigation')): ?>
						<div id="mitigation">
							<?php /*include(get_template_directory()."/page-templates/inc/info-box.php"); */ ?>						
							<?php the_field('survey_mitigation'); ?>							
						</div>
					<?php endif; ?>
					<?php if (get_field('survey_services')): ?>
						<div id="services"><?php the_field('survey_services'); ?></div>
					<?php endif; ?>
				</main>
				<?php include(get_template_directory()."/page-templates/inc/sidebar.php"); ?>
			</div>
		</section>	
		<?php include(get_template_directory()."/page-templates/inc/case-studies.php"); ?>	
    </div> 
<?php get_footer(); ?>