<?php
/*
Template Name: Page - Contact Template
*/
?>
<?php get_header(); $phone_number = get_option("phone_number"); $email_address = get_option("email_address"); $facebook = get_option("facebook"); $twitter = get_option("twitter"); $linkedin = get_option("linkedin"); ?>
	<div id="wrap">
		<?php
			$hero = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'hero'); $hero = $hero['0'];
			$title = get_the_title();
		?>
		<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
		<div class="contact-form">
			<div class="container">
				<h1>Get in touch</h1>
				<hr>
				<div class="row">					
					<div class="col intro">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; endif; ?>
					</div>
					<div class="col form">
						<?php the_field('contact_form_id'); ?>
					</div>					
				</div>
			</div>
		</div>
		<div id="find-us">
			<div class="container">
				<div class="row">
					<div class="col">
						<h2>Contact</h2>
					</div>
					<div class="col">
						<p><a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a></p>
					</div>
					<div class="col">
						<p>Telephone: <a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a></p>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col">
						<h2>Find us</h2>
					</div>
					<div class="col">
						<?php the_field('address'); ?>
					</div>
					<div class="col">
						<ul>
							<li><a href="<?php echo $facebook; ?>" target="_blank">Facebook</a></li>
							<li><a href="<?php echo $twitter; ?>" target="_blank">Twitter</a></li>
							<li><a href="<?php echo $linkedin; ?>" target="_blank">Linked In</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div> 
<?php get_footer(); ?>