<?php
/*
Template Name: Page - Calendar Template
*/
?>
<?php get_header(); $phone_number = get_option("phone_number"); ?>
	<div id="wrap">
		<?php
			$hero = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'hero'); $hero = $hero['0'];
			$title = get_the_title();
		?>
		<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
		<section class="main-content">
			<div class="container">	
				<main>
					<div class="intro">
						<h1><i class="fa fa-info" aria-hidden="true"></i><?php the_field('intro_title'); ?></h1>
						<div class="row">
							<div class="col">
								<div class="inner">
									<?php the_field('intro_box_left'); ?>
								</div>
							</div>
							<div class="col">
								<div class="inner">
									<?php the_field('intro_box_right'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="key">
						<h2>Key</h2>
						<ul>
							<li class="recommended">Recommended Survey Time</li>
							<li class="possible">Possible Survey Time</li>
						</ul>
					</div>
					<?php if( have_rows('calendar') ): ?>
						<?php while( have_rows('calendar') ): the_row(); ?>
							<div class="calendar">
								<h3><?php the_sub_field('title'); ?></h3>
								<div class="label"><?php the_sub_field('label'); ?> | <a href="<?php the_sub_field('link_label'); ?>"><?php the_sub_field('link_text'); ?></a></div>
								<?php if( have_rows('month') ): ?>
									<?php $monthName = ['J','F','M','A','M','J','J','A','S','O','N','D']; ?>
									<ul>
										<?php $count = 0; while( have_rows('month') ): the_row(); ?>
											<li<?php if( get_sub_field('survey_details') != 'None' ): ?> class ="<?php if( get_sub_field('survey_details') == 'Recommended Survey Time' ): ?>recommended<?php endif; ?><?php if( get_sub_field('survey_details') == 'Possible Survey Time' ): ?>possible<?php endif; ?><?php if( get_sub_field('duration') == 'Half Month (Start)' ): ?> half-start<?php endif; ?><?php if( get_sub_field('duration') == 'Half Month (End)' ): ?> half-end<?php endif; ?>"<?php endif; ?>><?php echo $monthName[$count] ?></li>
										<?php $count++; endwhile; ?>
									</ul>
								<?php endif; ?>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
					<div class="download-pdf">
						<a href="<?php the_field('pdf_download'); ?>" class="btn btn-basic" target="_blank">Download as PDF</a>
					</div>
				</main>
				<?php include(get_template_directory()."/page-templates/inc/sidebar.php"); ?>
			</div>
		</section>
	</div>  
<?php get_footer(); ?>