
<?php if (!$hero): $hero = 'http://mmenvironment.wpengine.com/wp-content/uploads/2017/03/bird.jpg'; endif; ?>
<div id="hero" class="img" style="background-image:url(<?php echo $hero; ?>);">
	<div class="container">
		<div class="inner">
			<h1><?php echo $title; ?></h1>
			<?php if ($body) :
				$hero_body = $body;
			else:
				if(get_field('hero_body')):
					$hero_body = get_field('hero_body');
				endif;
			endif; ?>
		 	<?php if($hero_body): ?>
				<p><?php echo $hero_body; ?></p>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php if ( function_exists('yoast_breadcrumb') ) : yoast_breadcrumb(' <div class="breadcrumbs"><div class="container"><p>','</p></div></div> '); endif; ?>