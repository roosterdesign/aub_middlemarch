<?php $post_objects = get_field('select_case_studies'); if( $post_objects ): ?>
	<section id="case-studies">
		<div class="container">
			<div class="intro">
				<h1><?php the_field('cs_title'); ?></h1>
				<?php the_field('cs_intro'); ?>
			</div>
			<div class="case-studies-list">
				<div class="row">
					<?php foreach( $post_objects as $post): ?>
						<div class="col case-study">
						<?php if (get_field('cs_banner_bg')): 
								$cs_banner_bg = get_field('cs_banner_bg')['sizes'][ 'cs_banner_bg' ];
							else:
								$cs_banner_bg = $trimmedAssetPath.'/img/blog-thumbnail-fallback.jpg';
							endif; ?>					
							<a href="<?php the_permalink(); ?>" class="banner" style="background-image:url(<?php echo $cs_banner_bg; ?>)">
								<div class="label">
									<h2><?php trim_content(get_field('cs_banner_title'), 25); ?><?php if ( get_field('cs_banner_location') ): ?> <small><?php trim_content(get_field('cs_banner_location'), 35); ?></small><?php endif; ?></h2>
								</div>
							</a>
						</div>
					<?php endforeach; wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>