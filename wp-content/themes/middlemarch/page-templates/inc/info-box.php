<?php
	$info_box_body = get_option("info_box_body");
	$info_box_link_text = get_option("info_box_link_text");
	$info_box_link = get_option("info_box_link");
?>
<div class="info-box">
	<i class="fa fa-info" aria-hidden="true"></i>
	<p><?php echo $info_box_body; ?></p>
	<p><a href="<?php echo $info_box_body; ?>"><?php echo $info_box_link_text; ?></a></p>
</div>