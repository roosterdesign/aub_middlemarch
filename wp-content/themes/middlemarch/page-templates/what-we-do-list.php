<?php
/*
Template Name: Page - What We Do (List Page) Template
*/
?>
<?php get_header(); ?>
	<div id="wrap">
		<?php
			$hero = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'hero'); $hero = $hero['0'];
			$title = get_the_title();
		?>
		<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
		<div class="service-list">
			<div class="container">
				<div class="ecology-surveys">

					<?php
						$leading_banner_image = get_field('leading_banner_image')['sizes'][ 'leading_banner_image' ];
					?>
					<a href="<?php the_field('leading_banner_link'); ?>" class="banner" style="background-image:url('<?php echo $leading_banner_image; ?>')">
						<div class="label">
							<h2><?php the_field('leading_banner_title'); ?></h2>
						</div>
					</a>
					<div class="row">						
						<div class="col intro">
							<?php the_field('leading_banner_intro'); ?>
						</div>
						<div class="col page-list">
							<ul>
								<?php wp_list_pages( array( 'title_li' => '', 'child_of' => get_field('leading_banner_list_id') ) ); ?>
							</ul>
						</div>
					</div>
				</div>
				<hr>
				<?php if( have_rows('other_services') ): ?>
					<?php while( have_rows('other_services') ): the_row();  ?>
						<div class="other-services">
							<div class="row">
								<?php if (get_sub_field('block_left_title')): ?>
									<div class="col">
										<?php $block_left_image = get_sub_field('block_left_image')['sizes'][ 'block_image' ]; ?>
										<a href="<?php the_sub_field('block_left_link'); ?>" class="banner" style="background-image:url('<?php echo $block_left_image; ?>')">
											<div class="label">
												<h2><?php the_sub_field('block_left_title'); ?></h2>
											</div>
										</a>
									</div>
								<?php endif; ?>
								<?php if (get_sub_field('block_right_title')): ?>
									<div class="col">
										<?php $block_right_image = get_sub_field('block_right_image')['sizes'][ 'block_image' ]; ?>
										<a href="<?php the_sub_field('block_right_link'); ?>" class="banner" style="background-image:url('<?php echo $block_right_image; ?>')">
											<div class="label">
												<h2><?php the_sub_field('block_right_title'); ?></h2>
											</div>
										</a>
									</div>
								<?php endif; ?>
							</div>
						</div>
					<?php endwhile; ?>						
				<?php endif; ?>
			</div>
		</div>
	</div>  
<?php get_footer(); ?>