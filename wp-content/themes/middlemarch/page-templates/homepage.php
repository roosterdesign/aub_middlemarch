<?php
/*
Template Name: Page - Homepage
*/
?>
<?php get_header(); $phone_number = get_option("phone_number"); ?>
	<div id="wrap">
		<!-- <div id="hero" class="img" style="background-image:url('<?php the_field('hero_image'); ?>');"> -->
		<div id="hero" class="img">
			<div class="container">
				<p><?php the_field('hero_sub-title'); ?></p>
				<h1><?php the_field('hero_title'); ?></h1>
			</div>
		</div>
		<?php if( have_rows('services_banners') ): ?>
			<section id="survey-overview">
				<div class="container">
					<div class="row">			
						<?php $count = 1; while( have_rows('services_banners') ): the_row();  ?>
							<div class="col <?php echo $count; ?>">
								<article <?php if (get_sub_field('banner_image')): ?> style="background-image: url('<?php the_sub_field('banner_image'); ?>');"<?php endif; ?>>
									<h2><?php the_sub_field('banner_title'); ?></h2>
									<?php the_sub_field('banner_intro'); ?>
									<a href="<?php the_sub_field('banner_link'); ?>" class="btn btn-basic">Find out more</a>
									<?php if ($count === 1): ?><img src="<?php echo $trimmedAssetPath; ?>/img/homepage/survey-box-breakout-1.png"><?php endif; ?>
								</article>
							</div>
						<?php $count++; endwhile; ?>
					</div>
				</div>
			</section>
		<?php endif; ?>
		<section id="survey-calendar-overview">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="inner">
							<h2><?php the_field('calendar_overview_title'); ?></h2>
							<?php the_field('calendar_overview_body'); ?>
							<a href="<?php the_field('calendar_overview_link'); ?>" class="btn">Find out more</a>
						</div>
					</div>			
					<div class="col img" style="background-image:url('<?php the_field('calendar_overview_image'); ?>');"></div>
					<div class="col">
						<div class="img" style="background-image:url('<?php the_field('contact_image'); ?>');"></div>
						<h2><?php the_field('contact_title'); ?></h2>
						<?php the_field('contact_body'); ?>
					</div>
				</div>
			</div>
		</section>
		<div class="main">
			<div class="container">				
				<div class="about-overview">
					<img src="<?php the_field('about_overview_image'); ?>" width="600" height="370">
					<div class="panel">
						<div class="inner">
							<h2><?php the_field('about_overview_title'); ?></h2>
							<?php the_field('about_overview_body'); ?>
							<a href="<?php the_field('about_overview_link'); ?>" class="btn">Find out more</a>
						</div>
					</div>
				</div>
				<div class="coverage-overview">
					<img src="<?php echo $trimmedAssetPath; ?>/img/homepage/coverage-newt.jpg" width="244" height="107" class="newt">
					<div class="panel">
						<h2><?php the_field('bottom_section_title'); ?></h2>
						<?php the_field('bottom_section_body'); ?>
						<a href="<?php the_field('bottom_section_link'); ?>" class="btn btn-basic">Find out more</a>
						<div class="cf"></div>
						<img src="<?php the_field('bottom_section_icons'); ?>" width="280" height="60">
					</div>
					<img src="<?php the_field('bottom_section_image'); ?>" class="map" width="650" height="680">
				</div>
			</div>
		</div>	  
	</div>  
<?php get_footer(); ?>