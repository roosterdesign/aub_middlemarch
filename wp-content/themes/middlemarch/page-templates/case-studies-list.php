<?php
/*
Template Name: Page - Case Studies List
*/
get_header(); ?>
	<div id="wrap">
		<?php $hero = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'hero'); $hero = $hero['0'];  ?>
		<?php if (!$hero): $hero = 'http://mmenvironment.wpengine.com/wp-content/uploads/2017/03/bird.jpg'; endif; ?>
		<div id="hero" class="img" style="background-image:url(<?php echo $hero; ?>);">
			<div class="container">
				<div class="inner">
					<h1><?php the_title(); ?></h1>
					<p><?php the_field('hero_body'); ?></p>
					<p class="case-studies-link white"><a href="#case-studies-list" class="btn btn-basic">Case Studies</a></p>					
				</div>
			</div>
		</div>
		<?php if ( function_exists('yoast_breadcrumb') ) : yoast_breadcrumb(' <div class="breadcrumbs"><div class="container"><p>','</p></div></div> '); endif; ?>

		<div class="container post-list">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>

		<?php endwhile; endif; ?>
		<hr>

			<div id="case-studies-list">
				<?php $the_query = new WP_Query( array( 'post_type' => 'case-studies', 'posts_per_page' => -1 ) ); if ( $the_query->have_posts() ) : ?>
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<article class="row">
							<div class="col img">
								<a href="<?php the_permalink(); ?>">
								<?php if (get_field('cs_banner_bg')): 
										$cs_banner_bg = get_field('cs_banner_bg')['sizes'][ 'blog_thumbnail' ];
									else:
										$cs_banner_bg = $trimmedAssetPath.'/img/blog-thumbnail-fallback.jpg';
									endif; ?>
									<img src="<?php echo $cs_banner_bg; ?>">
								</a>
								<div class="meta">
									<?php the_field('cs_banner_title'); ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php the_field('cs_banner_location'); ?>
								</div>
							</div>
							<div class="col">
								<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
								<p><?php trim_content(get_the_content(), 225); ?></p>
								<div class="cf"></div>
								<a href="<?php the_permalink(); ?>" class="btn btn-basic">Read more</a>
							</div>
						</article>
					<?php endwhile; ?>
					<?php else: ?>
						<p>Sorry, we currently have no case studies.</p>
					<?php endif; wp_reset_postdata(); ?>
			</div>
		</div>
    </div>  
<?php get_footer(); ?>