<?php $facebook = get_option("facebook"); $twitter = get_option("twitter"); $linkedin = get_option("linkedin"); ?>
<section id="get-a-quote">	
	<div class="container">	
		<p class="label">Enquire Online</p>
		<h1>Get a Quote</h1>
		<p>Looking for further information and advice,<br> or a hassle free, no obligation quote?</p>
		<a href="/contact-us" class="btn double-border">Get a Quote</a>
	</div>
</section>
<footer id="site-footer">
	<div class="container">
		<div class="mailing-list">
			<ul class="quick-links">
				<li class="find-us"><a href="/contact-us#find-us">Find Us</a></li>
				<li class="contact"><a href="/contact-us">Get in touch</a></li>
				<li class="news"><a href="/blog">Latest news</a></li>
			</ul>
			<hr>
			<div class="mailing-list-form">
				<h2>Join our mailing list</h2>
				<?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax=true tabindex="99"]'); ?>
			</div>
		</div>
		<ul class="social">
			<li><a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li><a href="<?php echo $twitter; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			<li><a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
		</ul>
		<nav><?php wp_nav_menu( array( 'menu' => 'Main Nav', 'container' => '', 'depth' => 1 ) ); ?></nav>
	</div>
	<div class="btm-footer">
		<div class="container">
			<p>Middlemarch Environmental <?php echo date('Y'); ?></p>
			<p>Website design and marketing by <a href="http://www.auburn.co.uk" target="_blank">Auburn Creative</a></p>
		</div>
	</div>	
</footer>
<?php wp_footer(); ?>
</body>
</html>