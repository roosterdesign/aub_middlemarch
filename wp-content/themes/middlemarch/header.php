<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes(); ?><!--<![endif]-->
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
        <!--[if lt IE 9]>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<![endif]-->  
	    <?php global $trimmedAssetPath; wp_head(); $facebook = get_option("facebook"); $twitter = get_option("twitter"); $linkedin = get_option("linkedin"); ?>
	    <!--
		/**
		 * @license
		 * MyFonts Webfont Build ID 3360664, 2017-03-16T09:53:07-0400
		 * 
		 * The fonts listed in this notice are subject to the End User License
		 * Agreement(s) entered into by the website owner. All other parties are 
		 * explicitly restricted from using the Licensed Webfonts(s).
		 * 
		 * You may obtain a valid license at the URLs below.
		 * 
		 * Webfont: Sagona-Light by Rene Bieder
		 * URL: http://www.myfonts.com/fonts/rene-bieder/sagona/light/
		 * Copyright: Copyright &#x00A9; 2016 by Ren&#x00E9; Bieder. All rights reserved.
		 * Licensed pageviews: 10,000
		 * 
		 * 
		 * License: http://www.myfonts.com/viewlicense?type=web&buildid=3360664
		 * 
		 * © 2017 MyFonts Inc
		*/

		-->
	</head>
<body <?php body_class(); ?>>
<?php $phone_number = get_option("phone_number"); ?>
<header id="site-header">
	<div class="container">		
		<a href="/" class="logo"><img src="<?php echo $trimmedAssetPath; ?>/img/header/logo.jpg" alt="Middlemarch Environmental" width="257" height="56"></a>
		<div class="call-us">
			<p>You can call us on</p>
			<a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a>
		</div>
		<div class="cf"></div>
		<ul class="mobile-nav">
			<li><a href="#" class="open-nav"><i class="fa fa-bars" aria-hidden="true"></i>All</a></li>
			<li><a href="/">Home</a></li>
			<li><a href="/what-we-do/ecology-surveys">Surveys</a></li>
			<li><a href="/contact-us">Contact us</a></li>
		</ul>		
		<div class="mask"></div>
		<nav class="main-nav">
			<a href="#" class="close">X CLOSE</a>
			<div class="cf"></div>
			<?php wp_nav_menu( array( 'menu' => 'Main Nav', 'container' => '' ) ); ?>
			<ul class="social">
				<li><a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="<?php echo $twitter; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			</ul>
		</nav>		
	</div>	
</header>