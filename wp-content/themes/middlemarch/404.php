<?php get_header(); ?>
	<div id="wrap">
		<?php
			$hero = wp_get_attachment_image_src(get_post_thumbnail_id(12), 'hero'); $hero = $hero['0'];
			$title = 'Error 404';
		?>
		<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>
		<section class="main-content">
			<div class="container">	
				<main>
					<h1>Page not found</h1>
					<p>Please <a href="/">click here</a> to return to the homepage.</p>
				</main>
				<?php include(get_template_directory()."/page-templates/inc/sidebar.php"); ?>
			</div>
		</section>
    </div>
<?php get_footer(); ?>