$(document).ready(function(){

	// Check if mobile
	var $isMobile = true;
	function checkMobile() {
		if ( $('.mobile-nav').is(':visible') ) {
			$isMobile = true;
		} else {
			$isMobile = false;
		}
	}


	// Mobile Nav
	function mobileNav() {
		checkMobile();

		if ($isMobile) {
			$('.mobile-nav .open-nav').bind('click', function(e) {
				e.preventDefault();
				$('.mask').fadeIn();
				$('.main-nav').animate({
					'left': '0'
				}, 1000, 'easeInOutExpo' );
				$('.main-nav .close').bind('click', function(e) {
					e.preventDefault();
					$('.mask').fadeOut();
					$('.main-nav').animate({
						'left': '-90%'
					}, 1000, 'easeInOutExpo', function() {
						$('.main-nav .dropdown').removeClass('open').children('ul').slideUp();
					});
				});
			});

			// Mobile Nav Dropdown
			$('.main-nav .dropdown').bind('click', function(e) {
				if ($(this).hasClass('open')) {
					$(this).removeClass('open').children('ul').stop().slideUp();			
				} else {
					$('.main-nav .dropdown').removeClass('open').children('ul').stop().slideUp();
					$(this).addClass('open').children('ul').stop().slideDown();
				}
			});

			$('.main-nav .dropdown a').click(function(e){
				e.stopPropagation();
			});

		} else {
			$('.mobile-nav .open-nav, .main-nav .dropdown, .main-nav .dropdown a').unbind();
		}

	};
	mobileNav();

	var doit;
	window.onresize = function(){
	  clearTimeout(doit);
	  doit = setTimeout(mobileNav, 500);
	};


	// Survey Page

	(function scrollToSection() {
		$('body.page-template-survey-template #hero .intro a').click(function(e){
			e.preventDefault();
			$target = $(this).attr('href');
			$('html, body').animate({
		        scrollTop: $($target).offset().top
		    }, 1200, 'easeInOutExpo');
		})
	})();


	// Expertise List Page

	(function scrollToSection() {
		$('body.page-template-case-studies-list #hero .case-studies-link a').click(function(e){
			e.preventDefault();
			$target = $(this).attr('href');
			$('html, body').animate({
		        scrollTop: $($target).offset().top-35
		    }, 1200, 'easeInOutExpo');
		})
	})();


	// Homepage Background Image Slideshow
	(function homeHeroSlider() {		
		$("body.home #hero").backstretch([
		      "/wp-content/uploads/2017/04/middlemarch-we-do-creative-ecological-solutions.jpg",
		      "/wp-content/uploads/2017/03/middlemarch-about-us.jpg"
		      ], {
		        fade: 850,
		        duration: 5000
		    }
		);
	})();
	
});