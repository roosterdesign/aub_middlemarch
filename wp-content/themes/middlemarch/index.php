<?php get_header(); ?>

	<div id="wrap">

		<?php
			$hero = wp_get_attachment_image_src(get_post_thumbnail_id(12), 'hero'); $hero = $hero['0'];
			if ( is_archive() ):
				$title = single_cat_title("", false);
			else:
				$title = single_post_title("", false);
			endif;
			$body = get_field('hero_body', 12);
		?>
		<?php include(get_template_directory()."/page-templates/inc/hero.php"); ?>

		<div class="container post-list">


			<?php if (have_posts()) : $count = 0; while (have_posts()) : the_post(); $count++; if ($count == 3) : ?>

				<article class="row">
					<div class="col img">
						<a href="<?php the_permalink(); ?>">
						<?php if ( has_post_thumbnail() ) :
								the_post_thumbnail('blog_thumbnail');
							else: ?>
								<img src="<?php echo $trimmedAssetPath; ?>/img/blog-thumbnail-fallback.jpg">
							<?php endif; ?>
						</a>
						<div class="meta">
							<span class="date"><?php echo get_the_date( 'j<\s\up>S</\s\up> F Y'); ?></span><?php echo get_the_category_list(); ?>
						</div>
					</div>
					<div class="col">
						<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
						<p><?php trim_content(get_the_content(), 225); ?></p>
						<div class="cf"></div>
						<a href="<?php the_permalink(); ?>" class="btn btn-basic">Read more</a>
					</div>
				</article>


				<div class="tweet">
					<i class="fa fa-twitter" aria-hidden="true"></i>
				   <?php
					$tweets = getTweets(1, 'MiddlemarchEco');
					if(is_array($tweets)){
					echo '<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>';
					foreach($tweets as $tweet){
						/* echo '<pre>' . var_dump($tweet) . '</pre>'; */
					    if($tweet['text']){
					        $the_tweet = $tweet['text'];
					        if(is_array($tweet['entities']['user_mentions'])){
					            foreach($tweet['entities']['user_mentions'] as $key => $user_mention){
					                $the_tweet = preg_replace(
					                    '/@'.$user_mention['screen_name'].'/i',
					                    '<a href="http://www.twitter.com/'.$user_mention['screen_name'].'" target="_blank">@'.$user_mention['screen_name'].'</a>',
					                    $the_tweet);
					            }
					        }
					        if(is_array($tweet['entities']['hashtags'])){
					            foreach($tweet['entities']['hashtags'] as $key => $hashtag){
					                $the_tweet = preg_replace(
					                    '/#'.$hashtag['text'].'/i',
					                    '<a href="https://twitter.com/search?q=%23'.$hashtag['text'].'&src=hash" target="_blank">#'.$hashtag['text'].'</a>',
					                    $the_tweet);
					            }
					        }
					        if(is_array($tweet['entities']['urls'])){
					            foreach($tweet['entities']['urls'] as $key => $link){
					                $the_tweet = preg_replace(
					                    '`'.$link['url'].'`',
					                    '<a href="'.$link['url'].'" target="_blank">'.$link['url'].'</a>',
					                    $the_tweet);
					            }
					        }					        
					        /*echo '
					        <div class="twitter_intents">
					            <p><a class="reply" href="https://twitter.com/intent/tweet?in_reply_to='.$tweet['id_str'].'">Reply</a></p>
					            <p><a class="retweet" href="https://twitter.com/intent/retweet?tweet_id='.$tweet['id_str'].'">Retweet</a></p>
					            <p><a class="favorite" href="https://twitter.com/intent/favorite?tweet_id='.$tweet['id_str'].'">Favorite</a></p>
					        </div>';
					        echo '
					        <p class="timestamp">
					            <a href="https://twitter.com/MiddlemarchEco/status/'.$tweet['id_str'].'" target="_blank">
					                '.date('h:i A M d',strtotime($tweet['created_at'])).'
					            </a>
					        </p>';*/
				        	echo '
					        	<p class="date"><a href="https://twitter.com/MiddlemarchEco/status/'.$tweet['id_str'].'" target="_blank">@Middlemarcheco</a> - '.date('d F Y ',strtotime($tweet['created_at'])).'</p>';
				        	echo '<p>'.$the_tweet.'</p>';
					    } else {
					        echo '
					        <br /><br />
					        <a href="http://twitter.com/MiddlemarchEco" target="_blank">Click here to read MiddlemarchEco\'S Twitter feed</a>';
					    }
					}
					}
			        ?>
				</div>


			<?php else: ?>

				<article class="row">
					<div class="col img">
						<a href="<?php the_permalink(); ?>">
							<?php if ( has_post_thumbnail() ) :
								the_post_thumbnail('blog_thumbnail');
							else: ?>
								<img src="<?php echo $trimmedAssetPath; ?>/img/blog-thumbnail-fallback.jpg">
							<?php endif; ?>
						</a>
						<div class="meta">
							<span class="date"><?php echo get_the_date( 'j<\s\up>S</\s\up> F Y'); ?></span><?php echo get_the_category_list(); ?>
						</div>
					</div>
					<div class="col">
						<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
						<p><?php trim_content(get_the_content(), 250); ?></p>
						<a href="<?php the_permalink(); ?>" class="btn btn-basic">Read more</a>
					</div>
				</article>

			<?php endif; endwhile; endif; ?>

		</div>

		<?php the_posts_pagination( array( 'screen_reader_text' => ' ', 'mid_size'  => 3, 'prev_text' => __( '<i class="fa fa-angle-left" aria-hidden="true"></i> Previous Page' ), 'next_text' => __( 'Next Page <i class="fa fa-angle-right" aria-hidden="true"></i>' ) ) ); ?>

	</div>

<?php get_footer(); ?>