'use strict';
 
var gulp = require('gulp'),
  less = require('gulp-less'),
  path = require('path'),
  cssmin = require('gulp-cssmin'),
  combineMq = require('gulp-combine-mq'),
  plumber = require('gulp-plumber'),
  notify = require("gulp-notify"),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename');

  var onError = function (err) {  
    notify.onError({
      title:    "Gulp",
      subtitle: "Failure!",
      message:  "Error: <%= error.message %>",
      sound:    "Beep"
    })(err);
    this.emit('end');
  };

// --------------------------------------------------------------------------------------------------------------------------------------------

// LESS

  // gulp.task('less', function() {
  //   return gulp.src('./wp-content/themes/middlemarch/css/src/styles.less')
  //     .pipe(plumber({errorHandler: onError}))
  //     .pipe(less({ paths: [ path.join(__dirname, 'less', 'includes') ] }))
  //     .pipe(combineMq())
  //     .pipe(cssmin())
  //     .pipe(rename({suffix: '.min'}))
  //     .pipe(gulp.dest('./wp-content/themes/middlemarch/css/dist/'));
  // });

// --------------------------------------------------------------------------------------------------------------------------------------------

// Main scripts / plugins file

  gulp.task('scripts', function() {
    gulp.src([
        './wp-content/themes/middlemarch/js/src/plugins/flexibility.js',
        './wp-content/themes/middlemarch/js/src/plugins/jquery.easing.js',
        './wp-content/themes/middlemarch/js/src/plugins/backstretch.js',
        './wp-content/themes/middlemarch/js/src/main.js'
      ])
      .pipe(plumber({errorHandler: onError}))
      .pipe(concat('main.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest('./wp-content/themes/middlemarch/js/dist/'))
  });

// --------------------------------------------------------------------------------------------------------------------------------------------

// Watch/Default Tasks

gulp.task('watch', function () {
  // gulp.watch(['./wp-content/themes/middlemarch/css/src/**/*.less'], ['less']);
  gulp.watch('./wp-content/themes/middlemarch/js/src/main.js', ['scripts']);
});

gulp.task('default', ['watch', 'scripts']);